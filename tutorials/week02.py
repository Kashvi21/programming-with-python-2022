def get_max(mylist):
    m = mylist[0]
    for value in mylist[1:]:
        if value > m:
            m = value
    return m


def process_list(mylist):
    for pos in range(len(mylist)):
        if mylist[pos] % 2 == 0:
            mylist[pos] /= 2
        else:
            mylist[pos] *= 2

        if pos % 7 == 0:
            mylist[pos] += 7
    return get_max(mylist)


def collatz(n):
    values = []
    while n not in values:
        values.append(n)
        if n % 2 == 0:
            n //= 2
        else:
            n = n * 3 + 1
    return values[-1]


def get_pi(n):
    pi = 0
    for i in range(1, n):
        pi += (-4 / (2 * i - 1)) * (-1)**i
    return pi


def smallest_k(n):
    k = 1
    while k**3 <= n:
        k += 1
    return k


def caesar(text, shift):
    encrypted_chars = []
    for pos, c in enumerate(text):
        if c == " ":
            encrypted_chars.append(" ")
        else:
            ascii_code = ord(c)
            ascii_offset = ascii_code - 65
            new_offset = (ascii_offset + shift) % 26
            encrypted_chars.append(chr(new_offset + 65))
    return "".join(encrypted_chars)


def reverse_complement(text):
    table = str.maketrans("ACGT", "TGCA", "N")
    text = text[::-1].translate(table)
    return text


def my_sort(mylist):
    for i in range(1, len(mylist)):
        val = mylist[i]
        j = i
        while (j > 0) and (mylist[j - 1] > val):
            mylist[j] = mylist[j - 1]
            j -= 1
        mylist[j] = val
    return mylist


def fibonacci(n):
    fibs = [1, 1]
    for i in range(n-2):
        fibs.append(fibs[-1] + fibs[-2])
    return fibs


print("get_max: ", get_max([312, -1123, 742, -9, 123]))
print("process_list: ", process_list(list(range(50))))
print("collatz: ", collatz(124124))
print("get_pi: ", get_pi(5000))
print("smallest_k: ", smallest_k(2314132123))
print("encrpyt: ", caesar("HELLO WORLD", 3))
print("encrpyt, shift=19: ", caesar("HELLO WORLD", 19))
print("encrpyt, shift=29: ", caesar("HELLO WORLD", 29))
print("Reverse complement: ", reverse_complement("ACGATCGATCGATTC"))
print("Sort: ", my_sort([9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5, -6, -7, -8, -9]))
print("Fibonacci: ", fibonacci(20))
